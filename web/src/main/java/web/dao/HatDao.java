package web.dao;

import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import jakarta.persistence.TypedQuery;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import web.dao.model.HatModel;
import web.dao.model.UserModel;
import web.dao.repository.HatRepository;
import web.service.TimeService;

import javax.sql.DataSource;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class HatDao {
    private final DataSource dataSource;
    private final TimeService timeService;
    private final HatRepository repo;

    @PersistenceContext
    EntityManager em;

    @Transactional
    public String insertHat(HatModel model) {
        String id = UUID.randomUUID().toString();
        model.setId(id);
        model.setCreatedAt(timeService.getNow());
        em.persist(model);
        return id;
    }

    public Optional<HatModel> findById(String id) {
        return Optional.ofNullable(em.find(HatModel.class, id));
    }

    public List<HatModel> getHatsByUser(UserModel user) {
        TypedQuery<HatModel> query =
                em.createQuery("select h from HatModel h where user = :user", HatModel.class);
        query.setParameter("user", user);
        return query.getResultList();
    }

    public List<HatModel> getHatsByUserId(String userId) {
        return repo.findAllByUserId(userId);
    }
}
