package web.web.controller;

import jakarta.servlet.http.HttpServletRequest;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import web.service.HatService;
import web.service.dto.HatDto;
import web.web.form.HatsForm;

import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("web/hats")
@AllArgsConstructor
public class HatWebController extends AbstractController {
    private final HatService hatService;

    @GetMapping
    public String hatsGet(Model model) {
        Optional<String> userId = getUserId();
        if (userId.isEmpty()) {
            return "redirect:/web/login";
        }
        List<HatDto> usersHats = hatService.getUsersHats(userId.get());

        List<HatsForm> hatsForms = usersHats.stream()
                .map(h -> new HatsForm(h.getName(), h.getSize()))
                .toList();

        model.addAttribute("hats", hatsForms);
        return "hats";
    }

}
