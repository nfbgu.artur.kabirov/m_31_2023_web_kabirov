package web.dao.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import web.dao.model.HatModel;

import java.util.List;

public interface HatRepository extends JpaRepository<HatModel, String> {
    List<HatModel> findAllByUserId(String userId);
}
