package web.service.converter;

import org.springframework.stereotype.Service;
import web.dao.model.HatModel;
import web.service.dto.HatDto;

@Service
public class HatModelToDtoConverter implements Converter<HatModel, HatDto> {
    @Override
    public HatDto convert(HatModel hatModel) {
        return new HatDto(
                hatModel.getUserId(),
                hatModel.getName(),
                hatModel.getSize());
    }
}
