package web.service.converter;

import org.springframework.stereotype.Service;
import web.dao.model.HatModel;
import web.service.dto.HatDto;

@Service
public class HatDtoToModelConverter implements Converter<HatDto, HatModel> {
    @Override
    public HatModel convert(HatDto hatDto) {
        return new HatModel(
                null,
                hatDto.getUserId(),
                null,
                null,
                hatDto.getName(),
                hatDto.getSize(),
                null
        );
    }
}
