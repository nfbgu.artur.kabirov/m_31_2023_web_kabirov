package web.api;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpSession;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpClientErrorException;
import web.service.HatService;
import web.service.dto.HatDto;

@RestController
@RequiredArgsConstructor
@RequestMapping(path = "/hat")
public class HatController {
    private final HatService hatService;

    @PostMapping(path = "insert/{name}/{size}")
    public ResponseEntity<String> insertHat(
            @PathVariable String name,
            @PathVariable Integer size,
            HttpServletRequest request) {
        HttpSession session = request.getSession();
        String userId = (String) session.getAttribute("userId");
        if (userId == null) {
            return ResponseEntity
                    .status(HttpStatus.UNAUTHORIZED)
                    .body("can not add hat");
        }

        HatDto hatDto = new HatDto(userId, name, size);
        String id = hatService.insertHat(hatDto);
        return ResponseEntity.ok(id);
    }

}
