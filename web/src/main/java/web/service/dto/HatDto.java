package web.service.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.time.LocalDateTime;

@AllArgsConstructor
@Data
public class HatDto {
    String userId;
    String name;
    Integer size;
}
