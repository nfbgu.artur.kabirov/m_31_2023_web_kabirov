package web.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import web.dao.HatDao;
import web.dao.model.HatModel;
import web.service.converter.HatDtoToModelConverter;
import web.service.converter.HatModelToDtoConverter;
import web.service.dto.HatDto;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class HatService {
    private final HatDao hatDao;
    private final HatDtoToModelConverter converter;
    private final HatModelToDtoConverter hatModelToDtoConverter;

    @Transactional
    public String insertHat(HatDto hatDto) {
        return hatDao.insertHat(converter.convert(hatDto));
    }

    public List<HatDto> getUsersHats(String userId) {
        return hatDao.getHatsByUserId(userId).stream()
                .map(hatModelToDtoConverter::convert)
                .collect(Collectors.toList());
    }


}
